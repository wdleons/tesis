api = "2"
core = "7.x"
projects[drupal][type] = "core"

projects[tesis_profile][type] = "profile"
projects[tesis_profile][download][type] = "git"
projects[tesis_profile][download][url] = "git@gitlab.com:wdleons/tesis.git"
projects[tesis_profile][download][branch] = "master"
projects[tesis_profile][download][submodule][] = init